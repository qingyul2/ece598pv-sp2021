use super::hash::{Hashable, H256};

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    nodes: Vec<H256>,
}

// #[derive(Debug, Default)]
// struct Node {
//     parent: &Node,
//     hash: H256,
//     left: &Node,
//     right: &Node,
// }

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        // let len;
        // if data.len()%2 == 0 {
        //     len = data.len();
        // } else {
        //     len = data.len() + 1;
        // }
        // // calculate internal node number
        // let mut accum: i32 = len, mut remain: i32 = 0, mut num: i32 = len;
        // while accum > 0 {
        //     if accum%2 == 1 && remain == 1 {
        //         num += (accum + 1)/2;
        //         remain = 0;
        //     } else if accum%2 == 1 && remain = 0 {
        //         num += accum/2;
        //         remain = 1;
        //     } else {
        //         num += accum/2;
        //     }
        // }
        // println("{} internal nodes", num);
        if data.len() == 0 {
            // return MerkleTree {nodes: &[H256([0u8; 32])]};
            return MerkleTree {nodes: vec![[0u8; 32].into()]};
        }
        let k = data.len().next_power_of_two();
        // let mut nodes = &[H256([0u8; 32]); 2*k - 1];
        // let mut nodes = Vec::with_capacity(2*k - 1);
        // for n in 0 .. 2*k - 1 {
        //     nodes.push([0u8; 32].into());
        // }
        let mut nodes = vec![[0u8; 32].into(); 2*k - 1];
        let mut i = k - 1;
        for t in data {
            nodes[i] = t.hash();
            i += 1;
        }
        if data.len()%2 == 1 {
            nodes[i] = nodes[i - 1];
        }

        for i in (0 .. (k as f32).log2().floor() as usize).rev() {
            let l = 2u32.pow(i as u32) as usize;
            for j in l - 1 .. 2*l - 1 {
                let left = nodes[2*j + 1];
                let right = nodes[2*j + 2];
                if left == ([0u8; 32].into()) && right == left {
                    nodes[j] = nodes[j - 1];
                    break;
                } else {
                    let mut join = ring::digest::Context::new(&ring::digest::SHA256);
                    join.update(left.as_ref());
                    join.update(right.as_ref());
                    nodes[j] = H256::from(join.finish());
                }
            }
        }

        MerkleTree {nodes}
    }

    pub fn root(&self) -> H256 {
        self.nodes[0]
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut pf = Vec::new();
        if self.nodes.len() == 0 {
            return pf;
        }
        let mut i = (self.nodes.len() - 1)/2 + index;
        while i > 0 {
            if i%2 == 0 {
                pf.push(self.nodes[i - 1]);
                i = (i - 2)/2;
            } else {
                pf.push(self.nodes[i + 1]);
                i = (i - 1)/2;
            }
        }
        pf
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    if index > leaf_size - 1 {
        return false;
    }
    let mut hash = *datum;
    let mut i = leaf_size.next_power_of_two() + index - 1;
    let mut j= 0;
    while i > 0 {
        let sib = proof[j];
        j += 1;
        let mut join = ring::digest::Context::new(&ring::digest::SHA256);
        if i%2 == 0 {
            join.update(sib.as_ref());
            join.update(hash.as_ref());
            i = (i - 2)/2;
        } else {
            join.update(hash.as_ref());
            join.update(sib.as_ref());
            i = (i - 1)/2;
        }
        hash = join.finish().into();
    }

    hash == *root
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
}
